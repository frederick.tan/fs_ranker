
import json
import multiprocessing
import sys

class FeaturesScoresProcess(multiprocessing.Process):
	def __init__(self, fs_config_file, task_queue, logger):
		multiprocessing.Process.__init__(self)
		self.task_queue = task_queue
		config_data = open(fs_config_file)
		data = json.load(config_data)
		config_data.close()
		self.feature_selectors = []
		for method in data['fsmethods']:
			sys.path.append('./' + method['directory'])
			module = __import__(method['filename'].replace('.py',''))
			self.feature_selectors.append(module)

	def run(self):
		proc_name = self.name
		#FAT-TODOprint '%s: Starting' % proc_name
		while True:
			task = self.task_queue.get()
			if task is None:
				# Poison pill means we should exit
				#FAT-TODOprint '%s: Exiting' % proc_name
				break
			#FAT-TODOprint task
#			fs_scores = self.get_fs_scores(task)
			self.task_queue.task_done()
		#FAT-TODOprint 'Exiting properly'
		return

	def get_fs_scores(self, data_arg):
		df = data_arg.df.apply(pd.to_numeric, errors='coerce').dropna().reset_index(drop = True)
		start_column = data_arg.start_column
		end_column = data_arg.end_column
		num_classes = data_arg.num_classes
		fs_scores = []
		for fs in self.feature_selectors:
			X = df.iloc[:, start_column:end_column + 1].astype(float)
			y = df[[columns[result_column]]].astype(float)
			self.logger.info('Shape of the DataFrame: ' + str(df.shape))
			self.logger.info('Shape of Training Set: ' + str(X.shape))
			self.logger.info('Shape of Target Values: ' + str(y.shape))
			labels = X.columns.values
			self.logger.info('Feature Selection: ' + fs.get_name())
			fs_scores.append(fs.get_features(X, y, labels, num_classes, self.logger))

		return fs_scores

