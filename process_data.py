
import csv
import json
import logging
import multiprocessing
import pandas as pd

from data_args import *

class ProcessDataProcess(multiprocessing.Process):
	def __init__(self, data, fs_queue, classifier_queue, logger):
		multiprocessing.Process.__init__(self)
		self.logger = logger
		self.logger.info(data['attribute']['count'])
		filename = data['directory'] + '/' + data['filename']
		df = pd.read_csv(filename)
		start_column = data['data_range']['start']
		end_column = data['data_range']['end']
		result_column = data['result_column']
		num_classes = data['num_classes']
		columns = df.columns
		df = df.apply(pd.to_numeric, errors='coerce').dropna().reset_index(drop = True)
		self.result_queue = multiprocessing.Queue()
		self.dataArgs = QueueManager().DataArgs(df, start_column, end_column, result_column, self.result_queue)
		self.fs_queue = fs_queue
		self.classifier_queue = classifier_queue

	def run(self):
		proc_name = self.name
		self.fs_queue.put(self.dataArgs)
		self.fs_queue.put('None Nona Nono Noni Nonu')
		#FAT-TODOprint '%s: Starting' % proc_name
		self.classifier_queue.put('None the wiser')
		while True:
			task = self.result_queue.get()
			if task is None:
				# Poison pill means we should exit
				#FAT-TODOprint '%s: Exiting' % proc_name
				break
			#FAT-TODOprint task
			fs_scores = get_fs_scores(task)
			self.task_queue.task_done()
		#FAT-TODOprint 'Exiting properly'
		return
