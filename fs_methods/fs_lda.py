#!/usr/bin/python
"""
Author: Frederick Tan
Description: LDA Feature Selection based on the implementation of David Kerry and based on this site: http://sebastianraschka.com/Articles/2014_python_lda.html
"""

from sklearn import preprocessing
import numpy as np

def get_features(X_orig, y_orig, labels, num_classes, logger):
	X = X_orig
	y = y_orig
	num_features = X.shape[1]
	num_instances = X.shape[0]
	#normalise data
	preprocessing.scale(X, axis=0, with_mean=True, with_std=True, copy=False)
	#mean_vectors
	np.set_printoptions(precision=4)
	mean_vectors = []
	#logger.set_subname('#LDA steps')
	#logger.info('#1: Compute n-vectors')
	#print num_classes
	#print('X', X)
	for cl in range(num_classes):
		#bool_arr = [y==cl]
		#print('values == cl', X[y.values==cl].values)
		mean_vectors.append(np.mean(X[y.values==cl].values, axis=0))
		#logger.debug('Mean Vector class %s: %s\n' %(cl, mean_vectors[cl]))
		#print('Mean Vector class %s: %s\n' %(cl, mean_vectors[cl]))

	#logger.info('#2: Compute Scatter Matrices')
	#logger.info('#2.1: Compute Within-Class Scatter Matrices')
	S_win = np.zeros((num_features, num_features))
	#print('mean_vectors', mean_vectors)
	for cl, mv in zip(range(1, num_features), mean_vectors):
		class_scatter_matrix = np.zeros((num_features, num_features))
		for index, row in X[y.values == cl].iterrows():
			row, mv = row.values.reshape(num_features, 1), mv.reshape(num_features, 1) #make column vectors
			class_scatter_matrix += (row-mv).dot((row-mv).T)
		S_win += class_scatter_matrix
	#logger.debug('within-class Scatter Matrix:\n' + str(S_win))

	#logger.info('#2.1: Compute Between-Class Scatter Matrices')
	overall_mean = np.mean(X.values, axis=0)
	S_bw = np.zeros((num_features,num_features))
	#print('S_bw out', S_bw)
	for i, mean_vec in enumerate(mean_vectors):
		#logger.debug(X[y.values == i+1])
		n = X[y.values == i+1].shape[0]
		mean_vec = mean_vec.reshape(num_features, 1) # make column vector
		#print('mean_vec', mean_vec, overall_mean)
		S_bw += n * (mean_vec - overall_mean).dot((mean_vec - overall_mean).T)
		#print('S_bw in', S_bw)
	#logger.info('between-class Scatter Matrix:\n' + str(S_bw))

	#logger.info('#3: Solve the generalized eigen-value')
	#print S_win
	#print S_bw
	eig_vals, eig_vecs = np.linalg.eig(np.linalg.pinv(S_win).dot(S_bw))
	for i in range(len(eig_vals)):
		eigvec_sc = eig_vecs[:,i].reshape(num_features,1)
		#logger.debug('\nEigenvector {}: \n{}'.format(i+1, eigvec_sc.real))
		#logger.debug('Eigenvalue {:}: {:.2e}'.format(i+1, eig_vals[i].real))
	for i in range(len(eig_vals)):
		eigv = eig_vecs[:,i].reshape(num_features, 1)
		#np.testing.assert_array_almost_equal(np.linalg.inv(S_win).dot(S_bw).dot(eigv),
		#				     eig_vals[i] * eigv,
		#				     decimal=6, err_msg='', verbose=True)

	#logger.info('#4: Selecting linear discriminants for the new feature subspace')
	#logger.info('#4.1: Sorting the eigenvectors by decreasing eigenvalues')
	#logger.info('Make a list of (eigenvalue, eigenvector) tuples')
	eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:,i]) for i in range(len(eig_vals))]

	#logger.info('Sort the (eigenvalue, eigenvector) tuples from high to low')
	eig_pairs = sorted(eig_pairs, key=lambda k: k[0], reverse=True)

	# Visually confirm that the list is correctly sorted by decreasing eigenvalues
	#for i in eig_pairs:
		#logger.debug(i[0])

	#logger.info('Express the explained variance as percentage')
	eigv_sum = sum(eig_vals)
	#for i,j in enumerate(eig_pairs):
		#logger.debug('eigenvalue {0:}: {1:.2%}'.format(i+1, (j[0]/eigv_sum).real))

	#logger.info('#4.2: Choosing k eigenvectors with the largest eigenvalues')
	W = np.hstack((eig_pairs[0][1].reshape(num_features,1), eig_pairs[1][1].reshape(num_features,1)))
	#logger.debug('Matrix W:\n' + str(W.real))

	features = []
	for i in range(num_features):
		features.append(0)

	#features = np.zeros(num_features)
	for i in range(num_features):
		feature = 0
		for j in range(len(eig_vals)):
			feature += eig_vals[j] * abs(eig_vecs[i][j])
		features[i] = feature
	return features

def get_name():
	return "Linear Discriminant Analysis"

