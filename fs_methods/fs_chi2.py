#!/usr/bin/python

from sklearn import preprocessing
from sklearn.feature_selection import chi2

def get_features(X_orig, y_orig, labels, num_classes, logger):
	prep = preprocessing.MinMaxScaler(feature_range=(0.0, 1.0), copy=True)
	X = X_orig
	y = y_orig
	X = X.astype(float)
	X = prep.fit_transform(X)
	num_features = X.shape[1]
	num_instances = X.shape[0]
	r_chi2, r_pval = chi2(X, y.values)
	feature_average = []
	for i in range(len(X[0])):
		feature_average.append(0)
	for i in range(len(X)):
		for j in range(len(feature_average)):
			feature_average[j] += X[i][j]
	for j in range(len(feature_average)):
		feature_average[j] = feature_average[j] / len(X)

	class_avg_per_feature = []
	counts_for_averaging = []
	for i in range(num_features):
		class_avg_per_feature.append([])
		counts_for_averaging.append([])
		for j in range(num_classes):
			#print('j',j)
			class_avg_per_feature[i].append(0)
			counts_for_averaging[i].append(0)
	for instance in range(num_instances):
		for feature in range(num_features):
			#print 'instance: ' + str(instance) + ' feature: ' + str(feature)
			#print 'y[instance]: ' + str(int(y[instance]))
			target_y = y.loc[instance][0].astype(int)
			#A = class_avg_per_feature[feature][int(y[instance])]
			#B = X[instance][feature]
			class_avg_per_feature[feature][target_y] += X[instance][feature]
			counts_for_averaging[feature][target_y] += 1
	for feature in range(num_features):
		for each_class in range(num_classes):
			if counts_for_averaging[feature][each_class] == 0:
				class_avg_per_feature[feature][each_class] = 0
			else:
				class_avg_per_feature[feature][each_class] /= counts_for_averaging[feature][each_class]
	#print('class avg per feature:', class_avg_per_feature)
	chi2_features = []
	for i in range(num_features):
		chi2_features.append(0)

	#print('num_classes', num_classes)
	for feature in range(num_features):
		chi2_feature = 0
		for each_class in range(num_classes):
			expected = feature_average[feature]
			observed = class_avg_per_feature[feature][each_class]
			chi2_feature += ((observed - expected) * (observed - expected))/expected
		#print('chi2_feature', chi2_feature)
		chi2_features[feature] = chi2_feature
	return  chi2_features

def get_name():
	return "chi-square"
