#!/usr/bin/python
"""
Author: Frederick Tan
Description: RFE Feature Selection using Scikit implementation
"""

from sklearn import preprocessing
from sklearn.feature_selection import RFE
from sklearn.svm import SVR
import numpy as np

def get_features(X_orig, y_orig, labels, num_classes, logger):
	X = X_orig
	y = np.ravel(y_orig)
	num_features = X.shape[1]
	num_instances = X.shape[0]
	#normalise data
	preprocessing.scale(X, axis=0, with_mean=True, with_std=True, copy=False)
	estimator =  SVR(kernel="linear")
	selector = RFE(estimator, 1, step=1)
	selector = selector.fit(X, y)
	features = list((num_features - selector.ranking_)*1.0/num_features)
	return features

def get_name():
	return "Recursive Feature Elimination"

