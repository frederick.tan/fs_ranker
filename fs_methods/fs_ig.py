#!/usr/bin/python

from sklearn import preprocessing

import math
import numpy as np

def get_features(X_orig, y_orig, labels, num_classes, logger):
	#normalise data
	X = X_orig
	y = y_orig
	num_features = X.shape[1]
	num_instances = X.shape[0]
	preprocessing.scale(X, axis=0,with_mean=True, with_std=True, copy=False)

	binarizedX = [[[]for _ in range(num_classes)] for _ in range(num_features)]
	for feature in range(num_features):
		for cl in range(num_classes):
			#sort values ascending
			instances_of_feature = zip(X.iloc[:,feature], y.values)
			instances_of_feature.sort()
			unique_values = np.arange(-1.0,1.0, 0.1)
			best_score = 0
			best_threshold = 0.0
			for uvalue in unique_values:
				score = score_threshold(uvalue , instances_of_feature, cl)
				if score > best_score:
					best_score = score
					best_threshold = uvalue
##           Binarised already? Fix the thresholding to handle
##           data that is already binarised
			binarizedX[feature][cl] = (map(lambda x,y:x>y, [i for i,j in instances_of_feature if j == cl], np.repeat(best_threshold, sum(y.values == cl))))

	ent_col = []
	ent_row = []

	for index, feature in enumerate(binarizedX):
		overall_tot = 0.0
		tot0 = 0.0
		tot1 = 0.0
		intermed_val = 0.0
		for cl in binarizedX[index]:
			tot0 += cl.count(0)
			tot1 += cl.count(1)
		overall_tot += (tot0 + tot1)
		intermed_val = -(logint(tot0)) - (logint(tot1))
		ent_col.append((intermed_val + logint(overall_tot)) / (overall_tot * math.log(2.0)))
	for index, feature in enumerate(binarizedX):
		overall_tot = 0.0
		intermed_val = 0.0
		for cl in binarizedX[index]:
			tot0 = cl.count(0)
			tot1 = cl.count(1)
			intermed_val += logint(tot0) + logint(tot1) - logint(tot0 + tot1)
			overall_tot += tot0 + tot1
		ent_row.append( -intermed_val / (overall_tot * math.log(2.0))  )
	features = []
	for i in range(num_features):
		features.append(0)
	#features = np.zeros(num_features)

	for i in range(len(ent_col)):
		features[i] = ent_col[i] - ent_row[i]

	return features

def score_threshold(threshold, data, key):
#data is formatted as:
# val1, class
# val2, class
#key is the class ID number to score
#threshold is the threshold to test
	below_threshold_count = 0
	l_ratio = 0.0
	r_ratio = 0.0

	for x in range(len(data)):
		##FAT-TODOprint str(data[x][0])+ " thresh = " + str(threshold)
		if (data[x][0] <= threshold):
			below_threshold_count += 1
			if data[x][1] == key:
				l_ratio += 1
		else:
			if data[x][1] == key:
				r_ratio += 1

	if below_threshold_count != 0:
		l_ratio = l_ratio/below_threshold_count

	if (len(data) - below_threshold_count) != 0:
		r_ratio = r_ratio/(len(data) - below_threshold_count)

	return np.abs(l_ratio - r_ratio)

def logint(num):
	if num <= 0 :
		return 0
	else :
		return float(num) * math.log(float(num))

def get_name():
	return "Information Gain"
