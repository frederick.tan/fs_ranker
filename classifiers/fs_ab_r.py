#!/usr/bin/python
"""
Author: Frederick Tan
"""
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import MinMaxScaler

from fs_geomean import *

import numpy as np
import sys

def get_score(X_train, X_test, y_train, y_test, num_classes):
	score = 0
	geometric_mean = 0
	classifier = AdaBoostClassifier(DecisionTreeClassifier(max_depth=2), n_estimators=600, learning_rate=1, algorithm="SAMME.R")
	y_train = np.ravel(y_train)
	y_test = np.ravel(y_test)
	scaling = MinMaxScaler(feature_range=(0,1)).fit(X_train)
	X_train = scaling.transform(X_train)
	X_test = scaling.transform(X_test)
	classifier.fit(X_train, y_train)
	score = classifier.score(X_test, y_test)
	y_predict = classifier.predict(X_test)
	geometric_mean_woz, geometric_mean = get_geo_score(y_predict, y_test, num_classes)

	return score, geometric_mean_woz, geometric_mean


def get_name():
	return "AdaBoost (Real)"
