#!/usr/bin/python
"""
Author: Frederick Tan
"""
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import MinMaxScaler

from fs_geomean import *

import numpy as np

def get_score(X_train, X_test, y_train, y_test, num_classes):
	y_train = np.ravel(y_train)
	y_test = np.ravel(y_test)
	#print y_train
	#print y_test
	classifier = DecisionTreeClassifier(max_features='sqrt')
	scaling = MinMaxScaler(feature_range=(0, 1)).fit(X_train)
	X_train = scaling.transform(X_train)
	X_test = scaling.transform(X_test)
	classifier.fit(X_train, y_train)
	score = classifier.score(X_test, y_test)
	#print('Decision Tree:', score)
	#true_pos = [int(i) for i,j in zip(classifier.predict(X_test), y_test) if i == j]
	#true_pos_bc = np.bincount(true_pos, minlength=num_classes)
	#y_test_bc = np.bincount(y_test.astype(int), minlength=num_classes)
	#print(true_pos[y_test_bc > 0], y_test_bc[y_test_bc > 0])
	#ratios = []
	#ratios = [float(i)/float(j) for i,j in zip(np.bincount(true_pos, minlength=num_classes), np.bincount(y_test.astype(int), minlength=num_classes)) and j != 0]
	#for i,j in zip(np.bincount(true_pos, minlength=num_classes), np.bincount(y_test.astype(int), minlength=num_classes)):
	#	if j > 0:
	#		ratios.append(float(i)/float(j))
	#geometric_mean = (reduce(lambda x,y: x*y, ratios))**(1.0/float(num_classes))

	y_predict = classifier.predict(X_test)
	geometric_mean_woz, geometric_mean = get_geo_score(y_predict, y_test, num_classes)

	return score, geometric_mean_woz, geometric_mean

def get_name():
	return "Decision Tree"
