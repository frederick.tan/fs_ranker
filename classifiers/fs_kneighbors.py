#!/usr/bin/python
"""
Author: Frederick Tan
"""
import numpy as np
import pylab as pl

from fs_geomean import *

from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler

def get_score(X_train, X_test, y_train, y_test, num_classes):
	classifier = KNeighborsClassifier(3)
	scaling = MinMaxScaler(feature_range=(0, 1)).fit(X_train)
	X_train = scaling.transform(X_train)
	X_test = scaling.transform(X_test)
	y_train = y_train.ravel()
	y_test = y_test.ravel()
	#print('Fitting KNN (X)', X_train , X_test)
	#print('Fitting KNN (y)', y_train , y_test)
	classifier.fit(X_train , y_train)
	score = classifier.score(X_test , y_test)
	#print('KNN score:', score)
	y_predict = classifier.predict(X_test)
	geometric_mean_woz, geometric_mean = get_geo_score(y_predict, y_test, num_classes)

	return score, geometric_mean_woz, geometric_mean


def get_name():
	return "KNeighbors Classifier"
