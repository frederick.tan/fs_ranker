#!/usr/bin/python

import argparse
import csv
import json
import logging
import multiprocessing
import numpy as np
import pandas as pd
import os
import sys
import threading

from fs_logger import FSLogger
from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold

import pylab as pl

# Define the parser
parser = argparse.ArgumentParser(description="Feature Selection")
parser.add_argument("--fsconfig", help="Name of the feature selection config file in JSON format")
parser.add_argument("--classifiers", help="Name of the classifier config file in JSON format")
parser.add_argument("--dataset", help="Name of the dataset list file in JSON format")
parser.add_argument("-v", "--verbosity", help="Debug loglevel", type=int, default=0)
parser.add_argument("-l", "--logfile", help="Log file", default="fs.log")
parser.add_argument("--thmin", help="Minimum threshold", type=float, default=0.05)
parser.add_argument("--thmax", help="Maximum threshold", type=float, default=1.00)
parser.add_argument("--thstep", help="Incremental Step", type=float, default=0.05)
parser.add_argument("--resultdir", help="Results Directory", default="./results")

# Global variables
LOGFILE = "./fs_ranker.log"

file_name_prefix = ""

# Utility functions
def generate_feature_sets(selected_feats):
	feat_sets = []
	#logger.info('Create the feature set for our experiment')
	#logger.info('#1 The features per FS algo (algo)')
	for i in range(len(selected_feats)):
		feat_sets.append(selected_feats[i])

	#logger.info('#2 The features per FS algo pair (e.g. LDA + IG)')
	feat_pair = []
	for i in range(len(selected_feats)):
		for j in range(i, len(selected_feats)):
			if j != i:
				feat_pair.append([val for val in selected_feats[i] if val in selected_feats[j]])

	feat_pair_all_intersect = []
	for i in range(len(feat_pair)):
		feat_pair_all_intersect += feat_pair[i]

	feat_union = []
	for i in range(len(feat_sets)):
		feat_union += feat_sets[i]

	feat_all_intersect = feat_sets[0]
	for i in range(1, len(feat_sets)):
		feat_all_intersect = [val for val in feat_all_intersect if val in feat_sets[i]]

	feat_sets.extend(feat_pair)
	feat_sets.append(list(set(feat_pair_all_intersect)))
	feat_sets.append(list(set(feat_union)))
	feat_sets.append(feat_all_intersect)

	return feat_sets

def classifier_thread(X, y, start_column, end_column, result_column, feat_th, feat_sets, num_classes, num_folds, data_filename):
	#feat_sets = generate_feature_sets(selected_features)
	df_columns = ['feat_set_id', 'classifier', 'fold_id', 'feat_th', 'score', 'geo_score']
	df_ra = pd.DataFrame(columns = df_columns)
	directory = results_dir + '/' + data_filename
	feat_th = round(feat_th, 2)
	#filename = file_name_prefix + data_filename + '_' + str("{0:.2f}".format(feat_th))
	filename = file_name_prefix + data_filename + '_' + str(feat_th)

	#X = df.iloc[:, start_column:end_column + 1].astype(float)
	#y = df[[columns[result_column]]].astype(float)
	#X = preprocessing.MinMaxScaler(feature_range=(0.0, 1.0), copy=True).fit_transform(X)
	#y = preprocessing.MinMaxScaler(feature_range=(0.0, 1.0), copy=True).fit_transform(y)

	##print('feat_sets', len(feat_sets), feat_sets)
	##print 'classifier'

	for feat_set, feat_index in zip(feat_sets, range(len(feat_sets))):
		##print(feat_set, feat_index)

		if feat_set == []: continue


		#data = X.iloc[:, feat_set]
		##print feat_set
		X_feat_set = X[:, feat_set]
		##print('X vs X_feat_set (X)', X)
		##print('X vs X_feat_set (X_feat_set)', X_feat_set)
		##print('feat_set', feat_set)
		#columns = data.columns
		##print X_feat_set

		#rint 'shape [df]: ' + str(df.shape)
		##print 'shape [X_feat_set]: ' + str(X_feat_set.shape)
		##print 'type [X_feat_set]: ' + str(type(X_feat_set))
		##print 'shape [y]: ' + str(y.shape)

		#FAT-TODO#print data
		skf = StratifiedKFold(n_splits=num_folds)

		#row, column = zip(*skf)
		fold_ix = 0
		#FAT-TODO#print '############'
		#for train_index, test_index, fold_ix in zip(row, column, range(len(skf))):
		for train_index, test_index in skf.split(X_feat_set, y):
			X_train, X_test = X_feat_set[train_index], X_feat_set[test_index]
			y_train, y_test = y[train_index], y[test_index]
			for classifier, index in zip(classifiers, range(len(classifiers))):
				#print classifier.get_name()
				real_index = (feat_index, index, fold_ix)
				##print('real_index', real_index)
				#result_actual[real_index], result_geo[real_index] = classifier.get_score(X_train, X_test, y_train, y_test, num_classes)
				actual_score, geo_score_woz, geo_score = classifier.get_score(X_train, X_test, y_train, y_test, num_classes)
				#print('result_actual: ', actual_score, 'result_geo_woz: ', geo_score_woz, 'result_geo: ', geo_score)
				df_ra = df_ra.append({'feat_set_id':feat_index, 'classifier':classifier.get_name(), 'fold_id':fold_ix, 'feat_th':feat_th, 'score':actual_score, 'geo_score_woz':geo_score_woz, 'geo_score':geo_score}, ignore_index=True)
			fold_ix = fold_ix + 1

	#FAT-TODO#print 'badabing'
	##print result_actual.shape
	##print result_geo.shape
	if not os.path.exists(directory):
		os.makedirs(directory)
	df_ra.to_csv(directory +'/' + filename + '.csv')
	#for i in result_actual:
	#	np.savetxt('./results/' + filename + "_actual.csv", i, fmt='%.5f', delimiter = ",")
	#for i in result_geo:
	#	np.savetxt('./results/' + filename + "_geo.csv", i, fmt='%.5f', delimiter = ",")

	#np.savetxt(filename + "_actual.csv", result_actual, fmt='%.5f', delimiter = ",")
	#np.savetxt(filename + "_geo.csv", result_geo, fmt='%.5f', delimiter = ",")
	#return result_actual, result_geo
	return df_ra

def experiment_thread(data, feature_selectors):
	global logger
	#logger.info(data['attribute']['count'])
	filename = data['directory'] + '/' + data['filename']
	df = pd.read_csv(filename)
	start_column = data['data_range']['start']
	end_column = data['data_range']['end']
	result_column = data['result_column']
	num_classes = data['num_classes']
	columns = df.columns
	df = df.apply(pd.to_numeric, errors='coerce').dropna().reset_index(drop = True)
	fs_scores = []
	for fs in feature_selectors:
		#y = df.iloc[:, result_column].astype(float)
		X = df.iloc[:, start_column:end_column + 1].astype(float)
		y = df[[columns[result_column]]].astype(float)
		#logger.info('Shape of the DataFrame: ' + str(df.shape))
		#logger.info('Shape of Training Set: ' + str(X.shape))
		#logger.info('Shape of Target Values: ' + str(y.shape))
		labels = X.columns.values
		#logger.info('Feature Selection: ' + fs.get_name())
		#print labels
		#print('FS: ', fs.get_name())
		fs_scores.append(fs.get_features(X, y, labels, num_classes, logger))
		#print fs_scores
		#break
	#X = df.iloc[:, start_column:end_column + 1].astype(float)
	#y = df[[columns[result_column]]].astype(float)
	#X = np.array(df.iloc[:, start_column:end_column + 1].astype(float))
	#y = np.array(df[[columns[result_column]]].astype(float))
	X = df.iloc[:, start_column:end_column + 1].astype(float).values
	y = df.iloc[:, result_column].astype(float).values
	##print('X, y', X, y)
	#y_p = df[[columns[result_column]]].astype(float).values
	#y_t = df[ result_column].astype(float).values
	##print(y_p, y_t)
	#return
	#X = df.iloc[:, start_column:end_column + 1].astype(float)
	#y = df[[result_column]]
	#prepare container for holding selected features (according to threshold)
	num_features = df.iloc[:, start_column:end_column + 1].shape[1]
	threads = []
	##FAT-TODO#print fs_scores
	last_feat_th = 0
	for th in np.arange(thmin, thmax, thstep):
		#FAT #FAT-TODO#print 'boo-yoo'
		selected_features = [[] for i in range(len(fs_scores))]
		feat_th = int(th * float(num_features - 1))
		if last_feat_th == feat_th:
			continue
		else:
			last_feat_th = feat_th
		for i, scores in zip(range(len(fs_scores)), fs_scores):
			real_threshold = list(sorted(scores, reverse = True))[feat_th]
			for feat_id, score in zip(range(num_features), scores):
				if score >= real_threshold:
					selected_features[i].append(feat_id)
		#print 'Filename: ' + os.path.basename(filename).split('.')[0]
		#t = multiprocessing.Process(target = classifier_thread, args=(X, y, start_column, end_column, result_column, th, selected_features, num_classes, 3, os.path.basename(filename).split('.')[0]))
		feat_sets = generate_feature_sets(selected_features)
		classifier_thread(X, y, start_column, end_column, result_column, th, feat_sets, num_classes, 3, os.path.basename(filename).split('.')[0])
		#FAT-TODO#print 'boo-ya'
		#threads.append(t)

	#for t in threads:
	#	t.start()

	#for t in threads:
	#	t.join()

	#FAT-TODO#print 'Ta-da'
	return

if __name__ == "__main__":
	global classifiers
	global logger
	global thmin
	global thmax
	global thstep
	global results_dir

	# Define initial values
	logfile = LOGFILE
	loglevel = 0

	args = parser.parse_args()

	if args.fsconfig:
		config_data = open(args.fsconfig)
		data = json.load(config_data)
		config_data.close()
		feature_selectors = []
		for method in data['fsmethods']:
			sys.path.append('./' + method['directory'])
			file_name_prefix += method['filename'].replace('fs_','').replace('.py','')
			file_name_prefix += "_"
			module = __import__(method['filename'].replace('.py',''))
			feature_selectors.append(module)

	if args.dataset:
		dataset_list = open(args.dataset)
		data = json.load(dataset_list)
		dataset_list.close()
		datasets = data['datasets']

	classifiers = []
	if args.classifiers:
		config_data = open(args.classifiers)
		data = json.load(config_data)
		config_data.close()
		for classifier in data['classifiers']:
			sys.path.append('./' + classifier['directory'])
			module = __import__(classifier['filename'].replace('.py',''))
			classifiers.append(module)

	loglevel = args.verbosity
	logfile = args.logfile
	thmin = args.thmin
	thmax = args.thmax
	thstep = args.thstep
	results_dir = args.resultdir

	logger = FSLogger('FS', loglevel, logfile)

	jobs = []
	for data in datasets:
		#process = multiprocessing.Process(target=experiment_thread, args=(data,feature_selectors))
		experiment_thread(data,feature_selectors)
		#jobs.append(process)

	#for j in jobs:
	#	j.start()

	#for j in jobs:
	#	j.join()

	#FAT-TODO#print 'The End'

