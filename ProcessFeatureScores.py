
import multiprocessing

class GenerateFeatureScores(multiprocessing.Process):
	def __init__(self, task_queue, result_queue):
		multiprocessing.Process.__init__(self)
		self.task_queue = task_queue
		self.result_queue =

	def run(self):

def experiment_thread(data, feature_selectors):
	global logger
	logger.info(data['attribute']['count'])
	filename = data['directory'] + '/' + data['filename']
	df = pd.read_csv(filename)
	start_column = data['data_range']['start']
	end_column = data['data_range']['end']
	result_column = data['result_column']
	num_classes = data['num_classes']
	columns = df.columns
	df = df.apply(pd.to_numeric, errors='coerce').dropna().reset_index(drop = True)
	fs_scores = []
	for fs in feature_selectors:
		#y = df.iloc[:, result_column].astype(float)
		X = df.iloc[:, start_column:end_column + 1].astype(float)
		y = df[[columns[result_column]]].astype(float)
		logger.info('Shape of the DataFrame: ' + str(df.shape))
		logger.info('Shape of Training Set: ' + str(X.shape))
		logger.info('Shape of Target Values: ' + str(y.shape))
		labels = X.columns.values
		logger.info('Feature Selection: ' + fs.get_name())
		fs_scores.append(fs.get_features(X, y, labels, num_classes, logger))
	#X = df.iloc[:, start_column:end_column + 1].astype(float)
	#y = df[[columns[result_column]]].astype(float)
	X = np.array(df.iloc[:, start_column:end_column + 1].astype(float))
	y = np.array(df[[columns[result_column]]].astype(float))
	#X = df.iloc[:, start_column:end_column + 1].astype(float)
	#y = df[[result_column]]
	#prepare container for holding selected features (according to threshold)
	num_features = df.iloc[:, start_column:end_column + 1].shape[1]
	threads = []
	print fs_scores
	for th in np.arange(thmin, thmax, thstep):
		print 'boo-yoo'
		selected_features = [[] for i in range(len(fs_scores))]
		feat_th = int(th * float(num_features - 1))
		for i, scores in zip(range(len(fs_scores)), fs_scores):
			real_threshold = list(sorted(scores, reverse = True))[feat_th]
			for feat_id, score in zip(range(num_features), scores):
				if score >= real_threshold:
					selected_features[i].append(feat_id)
		t = threading.Thread(target = classifier_thread, args=(df, start_column, end_column, result_column, feat_th, selected_features, num_classes, 5))
		print 'boo-ya'
		threads.append(t)
		t.start()
		#t.join()

	print 'Ta-da'
	return
