
from multiprocessing.managers import BaseManager

class QueueManager(BaseManager): pass

class DataArgs(object):
	def __init__(self, df, start_column, end_column, result_column, result_queue):
		self.df = df
		self.start_column = start_column
		self.end_column = end_column
		self.result_column = result_column
		self.result_queue = result_queue

class DataArgsClassifier(object):
	def __init__(self, df, start_column, end_column, result_column, feat_th, selected_features, num_classes, num_folds, result_queue):
		self.df = df
		self.start_column = start_column
		self.end_column = end_column
		self.result_column = result_column
		self.feat_th = feat_th
		self.selected_features = selected_features
		self.num_classes = num_classes
		self.num_folds = num_folds
		self.result_queue = result_queue

