import logging
import time

class FSLogger:

	def __init__(self, name, level, logfile):
		self.name = name
		self.level = self.get_logging_level(level)
		self.logfile = logfile
		self.timeStamp = time.strftime("%Y%m%d-%H%M%S")
		self.logfile += '_' + self.timeStamp
		self.elapsed = time.clock()
		self.subname = ''
		logging.basicConfig(level=self.level, filename=self.logfile, format='%(asctime)s %(message)s')

	def log(self, loglevel, message):
		self.timeStamp = time.strftime("%Y%m%d-%H%M%S")
		log_message = str(self.name) + ':' + self.subname + ':' + str(message)
		if loglevel == logging.INFO:
			logging.info(log_message)
		elif loglevel == logging.DEBUG:
			logging.debug(log_message)
		elif loglevel == logging.WARNING:
			logging.warning(log_message)
		elif loglevel == logging.ERROR:
			logging.error(log_message)
		elif loglevel == logging.CRITICAL:
			logging.critical(log_message)

	def info(self, message):
		self.log(logging.INFO, message)

	def debug(self, message):
		self.log(logging.DEBUG, message)

	def critical(self, message):
		self.log(logging.CRITICAL, message)

	def warning(self, message):
		self.log(logging.WARNING, message)

	def error(self, message):
		self.log(logging.ERROR, message)

	def set_subname(self, subname):
		self.subname = subname

	def clear_subname(self):
		self.subname = ''

	def get_logging_level(self, level):
		switcher = {
			0: logging.NOTSET,
			1: logging.CRITICAL,
			2: logging.ERROR,
			3: logging.WARNING,
			4: logging.INFO,
			5: logging.DEBUG
		}
		return switcher.get(level, 0)
