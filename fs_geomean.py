#!/usr/bin/python
"""
Author: Frederick Tan
"""
import numpy as np
import functools

def get_geo_score(y_predict, y_test, num_classes):
	true_pos = [i for i,j in zip(y_predict, y_test) if i == j]
	#for i,j in zip(np.bincount(true_pos, minlength=num_classes), np.bincount(y_test.astype(int), minlength=num_classes)):
	#	if i == 0:
	#		print('i is 0, j is', j)
	ratios = [float(i)/float(j) for i,j in zip(np.bincount(true_pos, minlength=num_classes), np.bincount(y_test.astype(int), minlength=num_classes)) if j > 0]
	geometric_mean_woz = (functools.reduce(lambda x,y: x*y, ratios))**(1.0/float(len(ratios)))
	ratios = [float(i)/float(j) for i,j in zip(np.bincount(true_pos, minlength=num_classes), np.bincount(y_test.astype(int), minlength=num_classes)) if i > 0]
	geometric_mean = (functools.reduce(lambda x,y: x*y, ratios))**(1.0/float(len(ratios)))

	return geometric_mean_woz, geometric_mean

